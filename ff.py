import sys, csv, string

matrix, names, source, sink = [], [], 0, 0

class Node:
	def __init__(self, index, parent=None):
		self.index = index
		self.parent = parent
		self.children = []

	def getIndex(self):
		return self.index

	def getParent(self):
		return self.parent

	def addChild(self, index):
		c = Node(index, self)
		self.children.append(c)
		return c

def readCSV(file):
	"""
	Reads values from a given csv file and stores them in global variables
		Matrix - values of the adjacency matrix
		Names - names of the vertices
		Source - index of the source in the matrix
		Sink - index of the sink in the matrix
	"""
	global matrix, names, source, sink
	csvreader = csv.reader(file, delimiter=',', quoting=csv.QUOTE_NONE)
	readNames, readSS = False, False
	for line in csvreader:
		for i in range(len(line)): line[i] = line[i].strip()
		if not readNames:
			names, readNames = line, True
		elif not readSS:
			source, sink, readSS = names.index(line[0]), names.index(line[1]), True
		else:
			line = map(int, line)
			matrix.append(line)
	return

def getPath():
	"""
	Using matrix, return an arbitrary path from source to sink as an array of indices
	Applies DFS and stops short once it reaches the sink
	If no path exists, return an empty array
	"""
	visited = [False] * len(names) #To avoid cycles
	parent = Node(source)
	sinkNode = None
	stack = [parent]

	while stack:
		foundSink, hasChildren = False, False
		for i in range(len(names)): #Check for children
			last = stack[-1].getIndex()
			elem = matrix[last][i]
			if elem and not visited[i]: #If non-visited child exists
				stack.append(stack[-1].addChild(i))
				if i == sink:
					sinkNode = stack[-1]
					foundSink = True
				visited[i], hasChildren = True, True
				break

		if foundSink: break
		if not hasChildren:
			stack.pop()

	path = []
	while (sinkNode != None):
		path.append(sinkNode.getIndex())
		sinkNode = sinkNode.getParent()

	return path[::-1]

def verify():
	"""
	Verifies if the user had input a valid source and sink node
	"""
	for i in range(len(names)):
		assert matrix[i][source] == 0, "Given source node is not valid"
		assert matrix[sink][i] == 0, "Given sink node is not valid"

def getCut():
	"""
	Get the subset of edges S, V - S where the cut should take place
	"""
	visited = [False] * len(names)
	queue, subsetS, subsetSV = [source], [source], []

	while queue:
		for i in range(len(names)):
			elem = matrix[queue[0]][i]
			if elem and not visited[i]:
				queue.append(i)
				visited[i] = True
				break
		
		queue.pop(0)

	for i in range(len(names)):
		if not subsetS.count(i):
			subsetSV.append(i)

	return (subsetS, subsetSV)

def getFlow():
	"""
	Computes the maximium flow of a directed graph
	"""
	flow = [[0] * len(names)] * len(names)
	maxFlow = 0
	path = getPath()
	while (path):
		#Look for least edge, add to flow and subtract from residual graph
		least = min(matrix[path[i]][path[i+1]] for i in range(len(path)-1))
		maxFlow += least
		for i in range(len(path)-1):
			flow[path[i]][path[i+1]] += least
			matrix[path[i]][path[i+1]] -= least
			matrix[path[i+1]][path[i]] += least
		path = getPath()

	subsets = getCut()

	return (maxFlow, subsets)

def main(argv):
	if (len(argv) != 1):
		print "Usage: python ff.py [<filename>]"
		sys.exit(1)

	filename = argv[0]
	try:
		csvfile = open(filename, 'r')
	except IOError:
		print "Invalid filename"
		sys.exit(1)

	readCSV(csvfile)
	csvfile.close()

	verify()

	result = getFlow()
	print "Maximum flow: " + str(result[0])
	print "Subset S: " + ', '.join(map(str, (names[i] for i in result[1][0])))
	print "Subset V - S: " + ', '.join(map(str, (names[i] for i in result[1][1])))

if __name__ == '__main__':
	main(sys.argv[1:])
